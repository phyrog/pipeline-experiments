terraform {
  backend "s3" {
    key = "state/environment.tfstate"
  }
}
