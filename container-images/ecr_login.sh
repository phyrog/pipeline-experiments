#!/usr/bin/env sh

region="$1"

password=$(aws ecr get-login-password --region="${region}")

echo "{\"password\": \"${password}\"}"
