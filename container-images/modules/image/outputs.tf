# output "uri" {
#     value = aws_ecr_repository.this.repository_url
# }

output "name" {
  value = var.name
}

output "jobs" {
  value = local.jobs
}
