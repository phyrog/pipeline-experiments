locals {
  dockerfiles = {
    for file in fileset("${var.file_prefix}/${var.name}", "**/Dockerfile.*") :
    basename(file) => {
      filename = basename(file)
      tag      = trimprefix(basename(file), "Dockerfile.")
    }
  }
  repo_prefix = "${data.aws_caller_identity.this.account_id}.dkr.ecr.${data.aws_region.this.name}.amazonaws.com"
  repo        = "${local.repo_prefix}/${aws_ecr_repository.this.name}"
}
data "aws_caller_identity" "this" {}
data "aws_region" "this" {}

resource "aws_ecr_repository" "this" {
  force_delete         = true
  name                 = "${var.prefix}/${var.name}"
  image_tag_mutability = var.tag_mutability

  image_scanning_configuration {
    scan_on_push = var.scan_on_push
  }

  tags = var.tags
}

locals {
  jobs = [for name, file in local.dockerfiles : {
    "${var.name}:${file.tag}" : {
      needs : []
      image : {
        name : "gcr.io/kaniko-project/executor:v1.17.0-debug"
        entrypoint : [""]
      }
      id_tokens : {
        OIDC_TOKEN : {
          aud : "https://gitlab.com"
        }
      }
      script : [
        "echo \"$${OIDC_TOKEN}\" > /kaniko/web_identity_token",
        "mkdir -p ~/.aws",
        "echo \"[profile oidc]\nrole_arn=$${ROLE_ARN}\nweb_identity_token_file=/kaniko/web_identity_token\" > ~/.aws/config",
        "echo '{\"credHelpers\":{\"${local.repo_prefix}\":\"ecr-login\"}}' > /kaniko/.docker/config.json",
        "/kaniko/executor --context \"${path.cwd}/${var.file_prefix}/${var.name}\" --dockerfile \"${path.cwd}/${var.file_prefix}/${var.name}/${name}\" --destination \"${local.repo}:${file.tag}\""
      ]
    }
  }]
}
