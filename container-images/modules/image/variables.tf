variable "prefix" {
    type = string
}

variable "name" {
    type = string
}

variable "file_prefix" {
    type = string
}

variable "scan_on_push" {
    type = bool
    default = false
}

variable "tag_mutability" {
    type = string
    default = "MUTABLE"
}

variable "tags" {
    type = map(string)
    default = {}
}