terraform {
  backend "s3" {
    key = "state/container-images.tfstate"
  }
  required_providers {
    external = {
      source = "hashicorp/external"
    }
  }
}

locals {
  files       = fileset("images", "**/Dockerfile.*")
  dockerfiles = toset([for i in local.files : dirname(i)])
}

module "image" {
  source = "./modules/image"

  for_each = local.dockerfiles

  name        = each.key
  prefix      = var.environment
  file_prefix = "images"
}

locals {
  pipeline = merge(flatten([for k, v in module.image : v.jobs])...)
}

resource "local_file" "pipeline" {
  content         = yamlencode(local.pipeline)
  filename        = "${path.root}/build-images.yml"
  file_permission = "0644"
}
